import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'drivers-list',
  template: `
    <table>
        <tr>
        <th>#</th>
        <th>name</th>
        <th>Edit</th>
        <th>Remove</th>
</tr>
    <tr *ngFor="let driver of drivers; let i=index">
        <th>i</th>
        <th><a href="" [routerLink]="[':id', { id: driver.id }]">{{ driver.firstName }}</a></th>
        <th><a href="" [routerLink]="['edit/:id', { id: driver.id }]">/</a></th>
        <th><span (click)="deleteDriver(driver)">x</span></th>
</tr>

    </table>
    
    <a [routerLink]="['new']">Add Driver</a>
  `
})
export class DriversListComponent {

  @Input('drivers') drivers;
  @Output() delete = new EventEmitter();

  deleteDriver(driver) {
    this.delete.emit(driver)
  }

}
