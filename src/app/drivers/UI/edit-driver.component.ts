import { Component, OnInit } from '@angular/core';
import { provideForms, disableDeprecatedForms,
        FormGroup, FormControl, REACTIVE_FORM_DIRECTIVES,
        Validators, FormBuilder } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { HTTP_PROVIDERS } from '@angular/http'

import { AppService } from '../../service'
import { DriverService } from '../service'
import { Driver } from "../models";

@Component({
    template: `
        <h1>Edit Driver</h1>
    <form [formGroup]="form"  novalidate>
    <input type="text" placeholder="name"
           formControlName="firstName"
           [(ngModel)]="driver.firstName">
<br>
<button type="submit">Save</button>
</form>

    <span (click)="updateDriver()">update</span>
    <pre>
    {{ driver | json }}
</pre>   
    `,
    directives: [REACTIVE_FORM_DIRECTIVES],
    providers: [
        disableDeprecatedForms(),
        provideForms(),
        DriverService,
        AppService,
        HTTP_PROVIDERS
    ]
})
export class EditDriverComponent implements OnInit{

    form: FormGroup;
    private driver: Driver = new Driver('','');
    private id: string;

    constructor(fb: FormBuilder, private router: ActivatedRoute, private driverService: DriverService){
        this.form = fb.group({
            firstName: ['']

        })
    }

    ngOnInit() {
       this.router.params.forEach((params: Params) => {
           this.id = params['id'];
           this.driverService.getDriver(this.id)
               .subscribe(driver =>{
                   this.driver = driver;
               })
       });
    }

  updateDriver() {
    this.driver.firstName = this.form.value;
    console.log(this.driver);
    this.driverService.updateDriver({firstName: 'Tesema', id: this.driver.id})
      .subscribe(res => console.log(res));
  }

}
