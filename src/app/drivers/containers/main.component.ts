/**
 * Created by root on 8/21/16.
 */
import { Component } from '@angular/core';
import { ROUTER_DIRECTIVES } from '@angular/router';

import { DriversComponent } from './drivers.component'

@Component({
  template: `
      <drivers>
      
      </drivers>
  `,
  directives: [
    ...ROUTER_DIRECTIVES,
    DriversComponent
  ]
})
export class MainComponent {

}
